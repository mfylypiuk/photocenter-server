package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.SaleDTO;
import photocenter.server.entity.model.Sale;
import photocenter.server.entity.model.Warehouse;
import photocenter.server.entity.request.CreateSaleRequest;
import photocenter.server.repo.SaleRepository;
import photocenter.server.repo.WarehouseRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SaleService {

    SaleRepository saleRepository;
    WarehouseRepository warehouseRepository;

    @Transactional(readOnly = true)
    public List<SaleDTO> getAllSales() {
        return saleRepository.findAll().stream()
                .map(SaleDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public SaleDTO createSale(CreateSaleRequest request) {
        Warehouse product = warehouseRepository.findById(request.getProduct().getId()).get();
        if(product.getCount() - request.getCount() < 0) {
            return null;
        }

        Sale sale = new Sale();
        sale.setProduct(request.getProduct());
        sale.setCount(request.getCount());
        sale.setPrice(request.getPrice());
        sale.setDate(request.getDate());
        sale.setShop(request.getShop());
        Sale createdSale = saleRepository.save(sale);
        product.setCount(product.getCount() - request.getCount());
        warehouseRepository.save(product);

        return new SaleDTO(createdSale);
    }

    @Transactional
    public void deleteSale(String saleId) {
        saleRepository.deleteById(saleId);
    }

    @Transactional(readOnly = true)
    public List<SaleDTO> findAll() {
        return saleRepository.findAll().stream()
                .map(SaleDTO::new)
                .collect(Collectors.toList());
    }
}
