package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.WarehouseDTO;
import photocenter.server.entity.model.Warehouse;
import photocenter.server.entity.request.CreateWarehouseRequest;
import photocenter.server.entity.request.UpdateWarehouseRequest;
import photocenter.server.repo.WarehouseRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WarehouseService {

    WarehouseRepository repository;

    @Transactional(readOnly = true)
    public List<WarehouseDTO> getAllWarehouses() {
        return repository.findAll().stream()
                .map(WarehouseDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public WarehouseDTO createWarehouse(CreateWarehouseRequest request) {
        Warehouse warehouse = new Warehouse();
        warehouse.setProduct(request.getProduct());
        warehouse.setPrice(request.getPrice());
        warehouse.setCount(request.getCount());
        warehouse.setCompany(request.getCompany());
        warehouse.setVendor(request.getVendor());
        warehouse.setProductType(request.getProductType());

        Warehouse createdWarehouse = repository.save(warehouse);
        return new WarehouseDTO(createdWarehouse);
    }

    @Transactional
    public void deleteWarehouse(String warehouseId) {
        repository.deleteById(warehouseId);
    }

    @Transactional
    public WarehouseDTO updateWarehouse(UpdateWarehouseRequest request) {
        if (repository.findById(request.getId()).isPresent()) {
            Warehouse existingWarehouse = repository.findById(request.getId()).get();

            existingWarehouse.setProduct(request.getProduct()) ;
            existingWarehouse.setPrice(request.getPrice());
            existingWarehouse.setCount(request.getCount());
            existingWarehouse.setCompany(request.getCompany());
            existingWarehouse.setVendor(request.getVendor());
            existingWarehouse.setProductType(request.getProductType());

            Warehouse updatedWarehouse = repository.save(existingWarehouse);
            return new WarehouseDTO(existingWarehouse.getId(), updatedWarehouse.getProduct(),updatedWarehouse.getProductType(),
                    updatedWarehouse.getPrice(), updatedWarehouse.getCount(), updatedWarehouse.getCompany(),
                    updatedWarehouse.getVendor());
        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public List<WarehouseDTO> findAll() {
        return repository.findAll().stream()
                .map(WarehouseDTO::new)
                .collect(Collectors.toList());
    }
}
