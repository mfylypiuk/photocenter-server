package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.OfficeDTO;
import photocenter.server.entity.model.Office;
import photocenter.server.entity.request.CreateOfficeRequest;
import photocenter.server.entity.request.UpdateOfficeRequest;
import photocenter.server.repo.OfficeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OfficeService {

    OfficeRepository repository;

    @Transactional(readOnly = true)
    public List<OfficeDTO> getAllOffices() {
        return repository.findAll().stream()
                .map(OfficeDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public OfficeDTO createOffice(CreateOfficeRequest request) {
        Office office = new Office();
        office.setAddress(request.getAddress());
        office.setBranchDependency(request.getBranchDependency());
        office.setName(request.getName());
        office.setNumberOfJobs(request.getNumberOfJobs());
        office.setType(request.getType());

        Office createdOffice = repository.save(office);
        return new OfficeDTO(createdOffice);
    }

    @Transactional
    public void deleteOffice(String officeId) {
        repository.deleteById(officeId);
    }

//    @Transactional
//    public OfficeDTO updateOffice(UpdateOfficeRequest request) {
//        return repository.findById(request.getId())
//                .map(OfficeDTO::new)
//                .orElseThrow(IllegalArgumentException::new);
//    }
    @Transactional
    public OfficeDTO updateOffice(UpdateOfficeRequest request) {
        if (repository.findById(request.getId()).isPresent()) {
            Office existingOffice = repository.findById(request.getId()).get();

            existingOffice.setType(request.getType());
            existingOffice.setName(request.getName());
            existingOffice.setAddress(request.getAddress());
            existingOffice.setNumberOfJobs(request.getNumberOfJobs());
            existingOffice.setBranchDependency(request.getBranchDependency());

            Office updatedOffice = repository.save(existingOffice);

            return new OfficeDTO(updatedOffice.getId(), updatedOffice.getType(), updatedOffice.getName(),
                    updatedOffice.getAddress(), updatedOffice.getNumberOfJobs(), updatedOffice.getBranchDependency());
        } else {
            throw new IllegalArgumentException();
        }
    }


    @Transactional(readOnly = true)
    public List<OfficeDTO> findAll() {
        return repository.findAll().stream()
                .map(OfficeDTO::new)
                .collect(Collectors.toList());
    }
}
