package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.DeliveryLogDTO;
import photocenter.server.entity.model.DeliveryLog;
import photocenter.server.entity.model.Warehouse;
import photocenter.server.entity.request.CreateDeliveryLogRequest;
import photocenter.server.repo.DeliveryLogRepository;
import photocenter.server.repo.WarehouseRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DeliveryLogService {

    DeliveryLogRepository deliveryLogRepository;
    WarehouseRepository warehouseRepository;

    @Transactional(readOnly = true)
    public List<DeliveryLogDTO> getAllDeliveries() {
        return deliveryLogRepository.findAll().stream()
                .map(DeliveryLogDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public DeliveryLogDTO createDelivery(CreateDeliveryLogRequest request) {
        DeliveryLog deliveryLog = new DeliveryLog();
        deliveryLog.setCount(request.getCount());
        deliveryLog.setDate(request.getDate());
        deliveryLog.setProduct(request.getProduct());
        deliveryLog.setVendor(request.getVendor());
        DeliveryLog createdDelivery = deliveryLogRepository.save(deliveryLog);

        Warehouse product = warehouseRepository.findById(request.getProduct().getId()).get();
        product.setCount(product.getCount() + request.getCount());
        warehouseRepository.save(product);

        return new DeliveryLogDTO(createdDelivery);
    }

    @Transactional
    public void deleteDelivery(String deliveryId) {
        deliveryLogRepository.deleteById(deliveryId);
    }

    @Transactional(readOnly = true)
    public List<DeliveryLogDTO> findAll() {
        return deliveryLogRepository.findAll().stream()
                .map(DeliveryLogDTO::new)
                .collect(Collectors.toList());
    }
}
