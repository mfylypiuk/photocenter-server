package photocenter.server.service;

import photocenter.server.entity.dto.StaffDTO;
import photocenter.server.entity.model.Staff;
import photocenter.server.entity.request.CreateStaffRequest;
import photocenter.server.entity.request.UpdateStaffRequest;
import photocenter.server.repo.StaffRepository;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StaffService {

    StaffRepository repository;

    @Transactional(readOnly = true)
    public List<StaffDTO> getAllStaff() {
        return repository.findAll().stream()
                .map(StaffDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public StaffDTO createStaff(CreateStaffRequest request) {
        Staff staff = new Staff();
        staff.setName(request.getName());
        staff.setPost(request.getPost());
        staff.setOffice(request.getOffice());

        Staff createdStaff = repository.save(staff);
        return new StaffDTO(createdStaff);
    }

    @Transactional
    public void deleteStaff(String staffId) {
        repository.deleteById(staffId);
    }

    @Transactional
    public StaffDTO updateStaff(UpdateStaffRequest request) {
        if (repository.findById(request.getId()).isPresent()) {
            Staff existingStaff = repository.findById(request.getId()).get();

            existingStaff.setName(request.getName());
            existingStaff.setName(request.getName());
            existingStaff.setPost(request.getPost());
            existingStaff.setOffice(request.getOffice());

            Staff updatedStaff = repository.save(existingStaff);
            return new StaffDTO(existingStaff.getId(), updatedStaff.getName(),
                    updatedStaff.getPost(), updatedStaff.getOffice());
        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public List<StaffDTO> findAll() {
        return repository.findAll().stream()
                .map(StaffDTO::new)
                .collect(Collectors.toList());
    }
}
