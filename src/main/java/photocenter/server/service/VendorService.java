package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.VendorDTO;
import photocenter.server.entity.model.Vendor;
import photocenter.server.entity.request.CreateVendorRequest;
import photocenter.server.entity.request.UpdateVendorRequest;
import photocenter.server.repo.VendorRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VendorService {

    VendorRepository repository;

    @Transactional(readOnly = true)
    public List<VendorDTO> getAllVendors() {
        return repository.findAll().stream()
                .map(VendorDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public VendorDTO createVendor(CreateVendorRequest request) {
        Vendor vendor = new Vendor();
        vendor.setCompany(request.getCompany());
        vendor.setGoodTypes(request.getGoodTypes());

        Vendor createdVendor = repository.save(vendor);
        return new VendorDTO(createdVendor);
    }

    @Transactional
    public void deleteVendor(String vendorId) {
        repository.deleteById(vendorId);
    }

    @Transactional(readOnly = true)
    public List<VendorDTO> findAll() {
        return repository.findAll().stream()
                .map(VendorDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public VendorDTO updateVendor(UpdateVendorRequest request) {
        if (repository.findById(request.getId()).isPresent()) {
            Vendor existingVendor = repository.findById(request.getId()).get();

            existingVendor.setCompany(request.getCompany());
            existingVendor.setGoodTypes(request.getGoodTypes());

            Vendor updatedVendor = repository.save(existingVendor);
            return new VendorDTO(existingVendor.getId(), existingVendor.getCompany(),
                    existingVendor.getGoodTypes());
        } else {
            return null;
        }
    }

}
