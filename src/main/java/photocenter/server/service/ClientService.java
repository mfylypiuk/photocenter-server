package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.ClientDTO;
import photocenter.server.entity.model.Client;
import photocenter.server.entity.request.CreateClientRequest;
import photocenter.server.entity.request.UpdateClientRequest;
import photocenter.server.repo.ClientRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClientService {

    ClientRepository repository;

    @Transactional(readOnly = true)
    public List<ClientDTO> getAllClients() {
        return repository.findAll().stream()
                .map(ClientDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public ClientDTO createClient(CreateClientRequest request) {
        Client client = new Client();
        client.setDiscount(request.getDiscount());
        client.setName(request.getName());
        client.setStatus(request.getStatus());
        client.setOffice(request.getOffice());

        Client createdClient = repository.save(client);
        return new ClientDTO(createdClient);
    }

    @Transactional
    public void deleteClient(String clientId) {
        repository.deleteById(clientId);
    }

    @Transactional
    public ClientDTO updateClient(UpdateClientRequest request) {
        if (repository.findById(request.getId()).isPresent()) {
            Client existingClient = repository.findById(request.getId()).get();

            existingClient.setName(request.getName());
            existingClient.setStatus(request.getStatus());
            existingClient.setDiscount(request.getDiscount());
            existingClient.setOffice(request.getOffice());

            Client updatedClient = repository.save(existingClient);
            return new ClientDTO(existingClient.getId(), updatedClient.getName(),
                    updatedClient.getStatus(), updatedClient.getDiscount(), updatedClient.getOffice(), 0);
        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public List<ClientDTO> findAll() {
        return repository.findAll().stream()
                .map(ClientDTO::new)
                .collect(Collectors.toList());
    }
}
