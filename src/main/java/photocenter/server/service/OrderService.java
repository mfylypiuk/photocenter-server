package photocenter.server.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.server.entity.dto.OrderDTO;
import photocenter.server.entity.model.Order;
import photocenter.server.entity.request.CreateOrderRequest;
import photocenter.server.entity.request.UpdateOrderRequest;
import photocenter.server.repo.OrderRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderService {

    OrderRepository repository;

    @Transactional(readOnly = true)
    public List<OrderDTO> getAllOrders() {
        return repository.findAll().stream()
                .map(OrderDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public OrderDTO createOrder(CreateOrderRequest request) {
        Order order = new Order();
        order.setClient(request.getClient());
        order.setDate(request.getDate());
        order.setDone(request.isDone());
        order.setOffice(request.getOffice());
        order.setPrint(request.getPrint());
        order.setService(request.getService());
        order.setShots(request.getShots());
        order.setTaken(request.isTaken());
        order.setUrgency(request.getUrgency());

        Order createdOrder = repository.save(order);
        return new OrderDTO(createdOrder);
    }

    @Transactional
    public void deleteOrder(String orderId) {
        repository.deleteById(orderId);
    }

    @Transactional
    public OrderDTO updateOrder(UpdateOrderRequest request) {
        if (repository.findById(request.getId()).isPresent()) {
            Order existingOrder = repository.findById(request.getId()).get();
            existingOrder.setClient(request.getClient());
            existingOrder.setDate(request.getDate());
            existingOrder.setDone(request.isDone());
            existingOrder.setOffice(request.getOffice());
            existingOrder.setPrint(request.getPrint());
            existingOrder.setService(request.getService());
            existingOrder.setShots(request.getShots());
            existingOrder.setTaken(request.isTaken());
            existingOrder.setUrgency(request.getUrgency());
            existingOrder.setPrice(request.getPrice());

            Order updatedOrder = repository.save(existingOrder);
            return new OrderDTO(existingOrder.getId(), updatedOrder.getPrice(),
                    updatedOrder.getClient(), updatedOrder.getOffice(), updatedOrder.getPrint(),
                    updatedOrder.getShots(), updatedOrder.getUrgency(), updatedOrder.getService(),
                    updatedOrder.getDate(), updatedOrder.isDone(), updatedOrder.isTaken());
        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public List<OrderDTO> findAll() {
        return repository.findAll().stream()
                .map(OrderDTO::new)
                .collect(Collectors.toList());
    }
}
