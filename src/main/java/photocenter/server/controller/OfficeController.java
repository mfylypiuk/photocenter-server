package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import photocenter.server.entity.dto.OfficeDTO;
import photocenter.server.entity.model.Office;
import photocenter.server.entity.request.CreateOfficeRequest;
import photocenter.server.entity.request.UpdateOfficeRequest;
import photocenter.server.repo.OfficeRepository;
import photocenter.server.service.OfficeService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.OFFICE_API_REFERENCE)
public class OfficeController {

    OfficeService officeService;

    private static final Logger log = Logger.getLogger(OfficeController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<OfficeDTO>> getAllOffices() {
        log.info("Getting offices list");
        return ResponseEntity.ok(officeService.getAllOffices());
    }

    @PostMapping("/create")
    public ResponseEntity<OfficeDTO> createOffice(@Valid @RequestBody CreateOfficeRequest request) {
        log.info("New "+ Office.class.getCanonicalName() + " " + request.getName() + " created");
        return ResponseEntity.ok(officeService.createOffice(request));
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteOffice(@RequestParam(value="id", required=true) String id) {
        officeService.deleteOffice(id);
        log.info("office with id "+id+ " was deleted");
    }

    @PutMapping("/update")
    public ResponseEntity<OfficeDTO> updateOffice(@Valid @RequestBody UpdateOfficeRequest request) {
        OfficeDTO officeDTO = officeService.updateOffice(request);
        log.info(String.format("Office with id = %s has been saved", officeDTO.getId()));
        return ResponseEntity.ok(officeDTO);
    }


    @GetMapping("/search")
    public ResponseEntity<List<OfficeDTO>> searchOffice(
            @RequestParam(value="id", required=false) String id,
            @RequestParam(value="type", required=false) String type,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="address", required=false) String address,
            @RequestParam(value="numberOfJobs", required=false) Integer numberOfJobs,
            @RequestParam(value="branchDependencyId", required=false) String branchDependencyId) {

        List<OfficeDTO> result = new ArrayList<>();

        for (OfficeDTO office: officeService.findAll()) {
            if(id != null) if(!id.equals(office.getId())) continue;
            if(type != null) if(!type.equals(office.getType())) continue;
            if(name != null) if(!name.equals(office.getName())) continue;
            if(address != null) if(!address.equals(office.getAddress())) continue;
            if(numberOfJobs != null) if(!numberOfJobs.equals(office.getNumberOfJobs())) continue;
            if(branchDependencyId != null) if(!branchDependencyId.equals(office.getBranchDependency().getId())) continue;
            log.info("Office "+office.getId()+" was found");

            result.add(office);
        }

        return ResponseEntity.ok(result);
    }
}
