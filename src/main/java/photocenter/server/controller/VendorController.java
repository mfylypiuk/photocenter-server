package photocenter.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import org.apache.log4j.Logger;
import photocenter.server.entity.dto.VendorDTO;
import photocenter.server.entity.model.Vendor;
import photocenter.server.entity.request.CreateVendorRequest;
import photocenter.server.entity.request.UpdateVendorRequest;
import photocenter.server.repo.VendorRepository;
import photocenter.server.service.VendorService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.VENDOR_API_REFERENCE)
public class VendorController {

    @Autowired
    VendorService vendorService;

    private static final Logger log = Logger.getLogger(VendorController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<VendorDTO>> getAllVendors() {
        log.info("Getting vendors list");
        return ResponseEntity.ok(vendorService.getAllVendors());
    }

    @PostMapping("/create")
    public ResponseEntity<VendorDTO> createVendors(@Valid @RequestBody CreateVendorRequest request) {
        return ResponseEntity.ok(vendorService.createVendor(request));
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteVendor(@RequestParam(value="id", required=true) String id) {
        vendorService.deleteVendor(id);
        log.info("vendor with id " +id+ " was deleted");
    }

    @PutMapping("/update")
    public ResponseEntity<VendorDTO> updateVendor(
            @RequestParam(value="id", required=true) String id,
            @Valid @RequestBody UpdateVendorRequest request) {
        VendorDTO vendorDTO = vendorService.updateVendor(request);

        if (vendorDTO != null) {
            log.info(String.format("vendor with id = %s has been updated", vendorDTO.getId()));
            return ResponseEntity.ok(vendorDTO);
        } else {
            log.error(String.format("vendor updating error id = %s", request.getId()));
            return ResponseEntity.notFound().build();
        }
    }
}
