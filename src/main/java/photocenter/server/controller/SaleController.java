package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import photocenter.server.entity.dto.SaleDTO;
import photocenter.server.entity.request.CreateSaleRequest;
import photocenter.server.service.SaleService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.SALE_API_REFERENCE)
public class SaleController {

    SaleService saleService;

    private static final Logger log = Logger.getLogger(SaleController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<SaleDTO>> getAllSales() {
        log.info("Getting sales list");
        return ResponseEntity.ok(saleService.getAllSales());
    }

    @PostMapping("/create")
    public ResponseEntity<SaleDTO> createSale(@Valid @RequestBody CreateSaleRequest request) {
        SaleDTO sale = saleService.createSale(request);
        if(sale == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok(sale);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteSale(@RequestParam(value="id", required=true) String id) {
        saleService.deleteSale(id);
        log.info("sale with id " +id+ " was deleted");
    }
}
