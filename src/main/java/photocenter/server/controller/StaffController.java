package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import org.apache.log4j.Logger;
import photocenter.server.entity.dto.StaffDTO;
import photocenter.server.entity.model.Staff;
import photocenter.server.entity.request.CreateStaffRequest;
import photocenter.server.entity.request.UpdateStaffRequest;
import photocenter.server.repo.StaffRepository;
import photocenter.server.service.StaffService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.STAFF_API_REFERENCE)
public class StaffController {

    StaffService staffService;

    private static final Logger log = Logger.getLogger(StaffController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<StaffDTO>> getAllStaff() {
        log.info("Getting staff list");
        return ResponseEntity.ok(staffService.getAllStaff());
    }

    @PostMapping("/create")
    public ResponseEntity<StaffDTO> createStaff(@Valid @RequestBody CreateStaffRequest request) {
        return ResponseEntity.ok(staffService.createStaff(request));
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteStaff(@RequestParam(value="id", required=true) String id) {
        staffService.deleteStaff(id);
        log.info("staff with id " +id+ " was deleted");
    }


    @PutMapping("/update")
    public ResponseEntity<StaffDTO> updateStaff(
            @RequestParam(value="id", required=true) String id,
            @Valid @RequestBody UpdateStaffRequest request) {
        StaffDTO staffDTO = staffService.updateStaff(request);

        if (staffDTO != null) {
            log.info(String.format("staff with id = %s has been updated", staffDTO.getId()));
            return ResponseEntity.ok(staffDTO);
        } else {
            log.error(String.format("staff updating error id = %s", request.getId()));
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/search")
    public List<StaffDTO> searchStaff(
            @RequestParam(value="id", required=false) String id,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="post", required=false) String post,
            @RequestParam(value="officeId", required=false) String officeId) {

        List<StaffDTO> result = new ArrayList<>();

        for (StaffDTO staff: staffService.findAll()) {
            if(id != null) if(!id.equals(staff.getId())) continue;
            if(name != null) if(!name.equals(staff.getName())) continue;
            if(post != null) if(!post.equals(staff.getPost())) continue;
            if(officeId != null) if(!officeId.equals(staff.getOffice().getId())) continue;

            log.info("Staff "+staff.getId()+" was found");

            result.add(staff);
        }

        return result;
    }
}
