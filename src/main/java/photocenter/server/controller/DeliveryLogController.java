package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import photocenter.server.entity.dto.DeliveryLogDTO;
import photocenter.server.entity.model.DeliveryLog;
import photocenter.server.entity.request.CreateDeliveryLogRequest;
import photocenter.server.service.DeliveryLogService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.DELIVERYLOG_API_REFERENCE)
public class DeliveryLogController {

    DeliveryLogService deliveryLogService;

    private static final Logger log = Logger.getLogger(DeliveryLogController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<DeliveryLogDTO>> getAllDeliveries() {
        log.info("Getting deliveries list");
        return ResponseEntity.ok(deliveryLogService.getAllDeliveries());
    }

    @PostMapping("/create")
    public ResponseEntity<DeliveryLogDTO> createDelivery(@Valid @RequestBody CreateDeliveryLogRequest request) {
        log.info("New "+ DeliveryLog.class.getCanonicalName() + " created");
        return ResponseEntity.ok(deliveryLogService.createDelivery(request));
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteDelivery(@RequestParam(value="id", required=true) String id) {
        deliveryLogService.deleteDelivery(id);
        log.info("delivery with id "+id+ " was deleted");
    }


    @GetMapping("/search")
    public ResponseEntity<List<DeliveryLogDTO>> searchDelivery(
            @RequestParam(value="id", required=false) String id,
            @RequestParam(value="vendorId", required=false) String vendorId,
            @RequestParam(value="productId", required=false) String productId,
            @RequestParam(value="count", required=false) Integer count,
            @RequestParam(value="date", required=false) Date date) {

        List<DeliveryLogDTO> result = new ArrayList<>();

        for (DeliveryLogDTO deliveryLog: deliveryLogService.findAll()) {
            if(id != null) if(!id.equals(deliveryLog.getId())) continue;
            if(vendorId != null) if(!vendorId.equals(deliveryLog.getVendor().getId())) continue;
            if(productId != null) if(!productId.equals(deliveryLog.getProduct().getId())) continue;
            if(count != null) if(!count.equals(deliveryLog.getCount())) continue;
            if(date != null) if(!date.equals(deliveryLog.getDate())) continue;

            log.info("Delivery "+deliveryLog.getId()+" was found");

            result.add(deliveryLog);
        }

        return ResponseEntity.ok(result);
    }
}
