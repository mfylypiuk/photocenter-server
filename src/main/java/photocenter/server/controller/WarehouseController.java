package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import photocenter.server.entity.dto.WarehouseDTO;
import photocenter.server.entity.request.CreateWarehouseRequest;
import photocenter.server.entity.request.UpdateWarehouseRequest;
import photocenter.server.service.WarehouseService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.WAREHOUSE_API_REFERENCE)
public class WarehouseController {

    WarehouseService warehouseService;

    private static final Logger log = Logger.getLogger(WarehouseController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<WarehouseDTO>> getAllWarehouses() {
        log.info("Getting warehouse list");
        return ResponseEntity.ok(warehouseService.getAllWarehouses());
    }

    @PostMapping("/create")
    public ResponseEntity<WarehouseDTO> createWarehouse(@Valid @RequestBody CreateWarehouseRequest request) {
        return ResponseEntity.ok(warehouseService.createWarehouse(request));
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteWarehouse(@RequestParam(value="id", required=true) String id) {
        warehouseService.deleteWarehouse(id);
        log.info("warehouse with id " +id+ " was deleted");
    }


    @PutMapping("/update")
    public ResponseEntity<WarehouseDTO> updateWarehouse(
            @RequestParam(value="id", required=true) String id,
            @Valid @RequestBody UpdateWarehouseRequest request) {
        WarehouseDTO warehouseDTO = warehouseService.updateWarehouse(request);

        if (warehouseDTO != null) {
            log.info(String.format("warehouse with id = %s has been updated", warehouseDTO.getId()));
            return ResponseEntity.ok(warehouseDTO);
        } else {
            log.error(String.format("warehouse updating error id = %s", request.getId()));
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/search")
    public List<WarehouseDTO> searchWarehouse(
            @RequestParam(value="id", required=false) String id,
            @RequestParam(value="product", required=false) String product,
            @RequestParam(value="productType", required=false) String productType,
            @RequestParam(value="price", required=false) Double price,
            @RequestParam(value="count", required=false) Integer count,
            @RequestParam(value="company", required=false) String company,
            @RequestParam(value="vendorId", required=false) String vendorId) {

        List<WarehouseDTO> result = new ArrayList<>();

        for (WarehouseDTO warehouseDTO: warehouseService.findAll()) {
            if(id != null) if(!id.equals(warehouseDTO.getId())) continue;
            if(product != null) if(!product.equals(warehouseDTO.getProduct())) continue;
            if(productType != null) if(!productType.equals(warehouseDTO.getProductType())) continue;
            if(price != null) if(!price.equals(warehouseDTO.getPrice())) continue;
            if(count != null) if(!count.equals(warehouseDTO.getCount())) continue;
            if(company != null) if(!company.equals(warehouseDTO.getCompany())) continue;
            if(vendorId != null) if(!vendorId.equals(warehouseDTO.getVendor().getId())) continue;

            log.info("Warehouse "+warehouseDTO.getId()+" was found");

            result.add(warehouseDTO);
        }

        return result;
    }
}
