package photocenter.server.controller;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import photocenter.server.Constants;
import photocenter.server.entity.model.Price;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.PRICE_API_REFERENCE)
public class PriceController {

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<Price> getPrices() {
        Gson gson = new Gson();
        Price price = Price.getPriceList();

        try (Reader priceListFile = new FileReader("price-list.json")) {
            // Convert JSON File to Java Object
            price = gson.fromJson(priceListFile, Price.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //String jsonString = gson.toJson(price);
        //System.out.println(jsonString);

        return ResponseEntity.ok(price);
    }
    /*

    // Java objects to File
        try (FileWriter writer = new FileWriter("price-list.json")) {
            gson.toJson(writer, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
     */
}
