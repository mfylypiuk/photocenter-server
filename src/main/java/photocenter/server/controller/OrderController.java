package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import photocenter.server.entity.dto.OrderDTO;
import photocenter.server.entity.model.Order;
import photocenter.server.entity.model.Print;
import photocenter.server.entity.model.Service;
import photocenter.server.entity.model.Shots;
import photocenter.server.repo.OrderRepository;
import photocenter.server.repo.PrintRepository;
import photocenter.server.repo.ServiceRepository;
import photocenter.server.repo.ShotsRepository;
import photocenter.server.service.OrderService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.ORDER_API_REFERENCE)
public class OrderController {

    OrderService orderService;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    PrintRepository printRepository;

    @Autowired
    ShotsRepository shotsRepository;

    @Autowired
    ServiceRepository serviceRepository;

    private static final Logger log = Logger.getLogger(OrderController.class);

    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<OrderDTO>> getAllOrders() {
        log.info("Getting offices list");
        List<OrderDTO> result = orderService.getAllOrders();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/create")
    public Order createOrder(@RequestBody Order order) {
        Print _print = null;
        Shots _shots = null;
        Service _service = null;

        if(order.getPrint() != null) {
            _print = printRepository.save(new Print(order.getPrint().getFormatType(), order.getPrint().getPaperType(),
                order.getPrint().getNumberOfPhotos(), order.getPrint().getNumberOfPhotosFromEachFrame()));
        }

        if (order.getShots() != null) {
            _shots = shotsRepository.save(new Shots(order.getShots().getCount(), order.getShots().getPaidDevelopment()));
        }

        if (order.getService() != null) {
            _service = serviceRepository.save(new Service(order.getService().getNumberOfPhotoForDocument(),
                    order.getService().getNumberOfPhotoForRestavration(), order.getService().getNumberOfArtPhotos(),
                    order.getService().isCamera(), order.getService().isPersonalPhotographer()));
        }

        Order _order = orderRepository.save(new Order(order.getPrice(), order.getClient(), order.getOffice(), _print,
                _shots, order.getUrgency(), _service, order.getDate(), order.isDone(), order.isTaken()));
        log.info("New order with id "+order.getId() + " created");

        return _order;
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteOrder(@RequestParam(value="id", required=true) String id) {
        Optional<Order> orderData = orderRepository.findById(id);

        if (orderData.isPresent()) {
            Order order = orderData.get();

            if (order.getPrint() != null) {
                printRepository.deleteById(order.getPrint().getId());
                log.info("Delete print with ID = " + order.getPrint().getId());
            }

            if (order.getShots() != null) {
                shotsRepository.deleteById(order.getShots().getId());
                log.info("Delete print with ID = " + order.getShots().getId());
            }

            if (order.getService() != null) {
                serviceRepository.deleteById(order.getService().getId());
                log.info("Delete service with ID = " + order.getService().getId());
            }

            orderRepository.deleteById(order.getId());

            log.info("Delete order with ID = " + order.getId());
        }
    }

    @PutMapping("/update")
    public ResponseEntity<Order> updateOrder(
            @RequestParam(value="id", required=true) String id,
            @RequestBody Order order) {
        Optional<Order> orderData = orderRepository.findById(order.getId());

        if (orderData.isPresent()) {
            Order _order = orderData.get();

            Print print = null;
            Shots shots = null;
            Service service = null;

            if(_order.getPrint() == null && order.getPrint() != null) {
                print = printRepository.save(new Print(order.getPrint().getFormatType(), order.getPrint().getPaperType(),
                    order.getPrint().getNumberOfPhotos(), order.getPrint().getNumberOfPhotosFromEachFrame()));
                order.setPrint(print);
            }
            else if(_order.getPrint() != null && order.getPrint() == null) {
                printRepository.deleteById(_order.getPrint().getId());
                order.setPrint(null);
            }

            if(_order.getShots() == null && order.getShots() != null) {
                shots = shotsRepository.save(new Shots(order.getShots().getCount(), order.getShots().getPaidDevelopment()));
                order.setShots(shots);
            }
            else if(_order.getShots() != null && order.getShots() == null) {
                shotsRepository.deleteById(_order.getShots().getId());
                order.setShots(null);
            }

            if(_order.getService() == null && order.getService() != null) {
                service = serviceRepository.save(new Service(order.getService().getNumberOfPhotoForDocument(),
                    order.getService().getNumberOfPhotoForRestavration(), order.getService().getNumberOfArtPhotos(),
                    order.getService().isCamera(), order.getService().isPersonalPhotographer()));
                order.setService(service);
            }
            else if(_order.getService() != null && order.getService() == null) {
                serviceRepository.deleteById(_order.getService().getId());
                order.setService(null);
            }

            _order = order;

            log.info("Update order with ID = " + order.getId());
            return new ResponseEntity<>(orderRepository.save(_order), HttpStatus.OK);
        } else {
            log.info("Error updating order with ID = " + order.getId());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/search")
    public List<Order> searchOrder(
        @RequestParam(value="id", required=false) String id,
        @RequestParam(value="clientId", required=false) String clientId,
        @RequestParam(value="officeType", required=false) String officeType,
        @RequestParam(value="clientType", required=false) String clientType,
        @RequestParam(value="officeId", required=false) String officeId,
        @RequestParam(value="orderType", required=false) String orderType,
        @RequestParam(value="urgency", required=false) Boolean urgency,
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        @RequestParam(value="dateMin", required=false) Date dateMin,
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        @RequestParam(value="dateMax", required=false) Date dateMax,
        @RequestParam(value="done", required=false) Boolean done,
        @RequestParam(value="taken", required=false) Boolean taken) {

        List<Order> result = new ArrayList<>();

        for (Order order: orderRepository.findAll()) {
            if(id != null) if(!id.equals(order.getId())) continue;
            if(clientId != null && order.getClient() != null) if(!clientId.equals(order.getClient().getId())) continue;
            if(officeType != null && order.getOffice() != null) if(!officeType.equals(order.getOffice().getType())) continue;
            if(clientType != null && order.getClient() != null) if(!clientType.equals(order.getClient().getStatus())) continue;
            if(officeId != null && order.getOffice() != null) if(!officeId.equals(order.getOffice().getId())) continue;
            if(urgency != null) if(urgency != order.getUrgency()) continue;

            if(dateMin != null && dateMax != null) {
                if(order.getDate().before(dateMin) || order.getDate().after(dateMax)) continue;
            }
            else if(dateMin != null && dateMax == null) {
                if(order.getDate().before(dateMin)) continue;
            }
            else if(dateMin == null && dateMax != null) {
                if(order.getDate().after(dateMax)) continue;
            }

            if(done != null) if(done != order.isDone()) continue;
            if(taken != null) if(taken != order.isTaken()) continue;
            log.info("Order "+order.getId()+" was found");

            result.add(order);
        }

        return result;
    }

}
