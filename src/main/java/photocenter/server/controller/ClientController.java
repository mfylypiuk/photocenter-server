package photocenter.server.controller;

import lombok.AllArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import photocenter.server.Constants;
import photocenter.server.entity.dto.ClientDTO;
import photocenter.server.entity.request.CreateClientRequest;
import photocenter.server.entity.request.UpdateClientRequest;
import photocenter.server.service.ClientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@AllArgsConstructor
@RequestMapping(Constants.API_SECURE_REFERENCE + Constants.CLIENT_API_REFERENCE)
public class ClientController {

    ClientService clientService;

    private static final Logger log = Logger.getLogger(ClientController.class);


    @GetMapping(value = {"", "/", "/list"})
    public ResponseEntity<List<ClientDTO>> getAllClients() {
        log.info("Getting clients list");
        return ResponseEntity.ok(clientService.getAllClients());
    }

    @PostMapping("/create")
    public ResponseEntity<ClientDTO> createClient(@Valid @RequestBody CreateClientRequest request) {
        return ResponseEntity.ok(clientService.createClient(request));
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete")
    public void deleteClient(@RequestParam(value="id", required=true) String id) {
        clientService.deleteClient(id);
        log.info("client with id " +id+ " was deleted");
    }

    @PutMapping("/update")
    public ResponseEntity<ClientDTO> updateClient(
            @RequestParam(value="id", required=true) String id,
            @Valid @RequestBody UpdateClientRequest request) {
        ClientDTO clientDTO = clientService.updateClient(request);

        if (clientDTO != null) {
            log.info(String.format("client with id = %s has been updated", clientDTO.getId()));
            return ResponseEntity.ok(clientDTO);
        } else {
            log.error(String.format("client updating error id = %s", request.getId()));
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/search")
    public ResponseEntity <List<ClientDTO>> searchClient(
            @RequestParam(value="id", required=false) String id,
            @RequestParam(value="name", required=false) String name,
            @RequestParam(value="status", required=false) String status,
            @RequestParam(value="discount", required=false) Boolean discount,
            @RequestParam(value="officeId", required=false) String officeId) {

        List<ClientDTO> result = new ArrayList<>();

        for (ClientDTO client: clientService.findAll()) {
            if(id != null) if(!id.equals(client.getId())) continue;
            if(name != null) if(!name.equals(client.getName())) continue;
            if(status != null) if(!status.equals(client.getStatus())) continue;
            if(discount != null) if(discount != client.getDiscount()) continue;
            if(discount != null) if(discount != client.getDiscount()) continue;
            if(officeId != null) if(!officeId.equals(client.getOffice().getId())) continue;
            log.info("Client "+client.getId()+" was found");

            result.add(client);
        }

        return ResponseEntity.ok(result);
    }


}

