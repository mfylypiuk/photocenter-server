package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Office;

@Repository
public interface OfficeRepository extends MongoRepository<Office, String> {
}
