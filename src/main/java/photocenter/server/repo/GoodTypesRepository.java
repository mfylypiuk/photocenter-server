package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.GoodTypes;

@Repository
public interface GoodTypesRepository extends MongoRepository<GoodTypes, String> {
}
