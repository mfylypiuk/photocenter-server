package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Staff;

@Repository
public interface StaffRepository extends MongoRepository<Staff, String> {
}