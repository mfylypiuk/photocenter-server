package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Client;

@Repository
public interface ClientRepository extends MongoRepository<Client, String> {

}
