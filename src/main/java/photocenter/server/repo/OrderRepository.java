package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Order;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {

}
