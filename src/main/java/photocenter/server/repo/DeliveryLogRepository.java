package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.DeliveryLog;

@Repository
public interface DeliveryLogRepository extends MongoRepository<DeliveryLog, String> {
}
