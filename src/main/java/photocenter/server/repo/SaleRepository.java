package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Sale;

@Repository
public interface SaleRepository extends MongoRepository<Sale, String> {
}
