package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Shots;

@Repository
public interface ShotsRepository extends MongoRepository<Shots, String> {

}