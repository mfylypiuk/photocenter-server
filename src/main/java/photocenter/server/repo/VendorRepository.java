package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Vendor;

@Repository
public interface VendorRepository extends MongoRepository<Vendor, String>  {
}
