package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Service;

@Repository
public interface ServiceRepository extends MongoRepository<Service, String> {

}

