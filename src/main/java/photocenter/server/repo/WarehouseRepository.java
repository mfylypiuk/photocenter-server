package photocenter.server.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import photocenter.server.entity.model.Warehouse;

@Repository
public interface WarehouseRepository extends MongoRepository<Warehouse, String> {
}
