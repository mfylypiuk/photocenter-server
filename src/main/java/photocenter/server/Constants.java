package photocenter.server;

public final class Constants {
    // EXAMPLE:
    // @RequestMapping(Constants.API_SECURE_REFERENCE + Constants.BRANCH_API_REFERENCE)
    public static final String API_SECURE_REFERENCE = "/api";
    public static final String CLIENT_API_REFERENCE = "/ClientService";
    public static final String OFFICE_API_REFERENCE = "/OfficeService";
    public static final String ORDER_API_REFERENCE = "/OrderService";
    public static final String SALE_API_REFERENCE = "/SaleService";
    public static final String STAFF_API_REFERENCE = "/StaffService";
    public static final String VENDOR_API_REFERENCE = "/VendorService";
    public static final String WAREHOUSE_API_REFERENCE = "/WarehouseService";
    public static final String DELIVERYLOG_API_REFERENCE = "/DeliveryService";
    public static final String PRICE_API_REFERENCE = "/PriceService";
}
