package photocenter.server.entity.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import photocenter.server.entity.model.GoodTypes;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateVendorRequest {

    @NotBlank
    String id;

    @NotBlank
    String company;

    @NonNull
    GoodTypes goodTypes;
}
