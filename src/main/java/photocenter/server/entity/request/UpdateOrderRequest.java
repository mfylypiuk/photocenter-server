package photocenter.server.entity.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.*;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateOrderRequest {

    @NotBlank
    String id;

    Double price;

    Client client;

    Office office;

    Print print;

    Shots shots;

    Boolean urgency;

    Service service;

    Date date;

    @NotBlank
    boolean done;

    @NotBlank
    boolean taken;
}
