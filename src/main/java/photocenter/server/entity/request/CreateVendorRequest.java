package photocenter.server.entity.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.GoodTypes;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateVendorRequest {

    @NotBlank
    String company;
    GoodTypes goodTypes;
}
