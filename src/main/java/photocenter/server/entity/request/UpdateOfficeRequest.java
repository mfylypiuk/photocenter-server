package photocenter.server.entity.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateOfficeRequest {

    @NotBlank
    String id;

    @NotBlank
    String type;

    @NotBlank
    String name;

    @NotBlank
    String address;

    @NotNull
    @Positive
    Integer numberOfJobs;

    Office branchDependency;

}
