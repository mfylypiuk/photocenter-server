package photocenter.server.entity.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateStaffRequest {

    @NotBlank
    String name;

    @NotBlank
    String post;

    Office office;
}
