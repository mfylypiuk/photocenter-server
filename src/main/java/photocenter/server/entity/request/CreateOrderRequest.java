package photocenter.server.entity.request;

import com.mongodb.lang.NonNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import photocenter.server.entity.model.*;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateOrderRequest {

    Double price;

    Client client;

    Office office;

    Print print;

    Shots shots;

    Boolean urgency;

    Service service;

    Date date;

    @NotBlank
    boolean done;

    @NotBlank
    boolean taken;
}
