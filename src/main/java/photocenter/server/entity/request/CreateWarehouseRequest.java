package photocenter.server.entity.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Vendor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateWarehouseRequest {

    @NotBlank
    String product;

    @NotBlank
    String productType;

    @NotNull
    Double price;

    @NotNull
    @Positive
    Integer count;

    @NotBlank
    String company;

    Vendor vendor;

}
