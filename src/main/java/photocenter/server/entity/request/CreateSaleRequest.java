package photocenter.server.entity.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;
import photocenter.server.entity.model.Warehouse;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateSaleRequest {

    Warehouse product;

    Integer count;

    Double price;

    Date date;

    Office shop;
}
