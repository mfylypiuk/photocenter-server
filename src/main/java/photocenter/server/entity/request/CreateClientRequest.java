package photocenter.server.entity.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateClientRequest {

    @NotBlank
    String name;

    @NotBlank
    String status;

    Boolean discount;

    Office office;
}
