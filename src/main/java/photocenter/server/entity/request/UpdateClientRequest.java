package photocenter.server.entity.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateClientRequest {

    @NotBlank
    String id;

    @NotBlank
    String name;

    @NotBlank
    String status;

    Boolean discount;

    @NotNull
    Office office;
}