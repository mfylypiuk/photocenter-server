package photocenter.server.entity.request;

import com.mongodb.lang.NonNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Vendor;
import photocenter.server.entity.model.Warehouse;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateDeliveryLogRequest {

    Vendor vendor;

    Warehouse product;

    @NonNull
    Integer count;

    Date date;
}
