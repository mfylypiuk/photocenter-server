package photocenter.server.entity.dto;

import com.mongodb.lang.NonNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import photocenter.server.entity.model.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderDTO {

    String id;
    Double price;
    Client client;
    Office office;
    Print print;
    Shots shots;
    Boolean urgency;
    Service service;
    Date date;
    boolean done;
    boolean taken;

    public OrderDTO(Order order) {
        this.id = order.getId();
        this.price = order.getPrice();
        this.date = order.getDate();
        this.client = order.getClient();
        this.office = order.getOffice();
        this.print = order.getPrint();
        this.shots = order.getShots();
        this.urgency = order.getUrgency();
        this.service = order.getService();
        this.done = order.isDone();
        this.taken = order.isTaken();
    }
}
