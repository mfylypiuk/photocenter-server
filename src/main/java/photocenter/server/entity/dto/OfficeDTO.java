package photocenter.server.entity.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import photocenter.server.entity.model.Office;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OfficeDTO {

    String id;
    String type;
    String name;
    String address;
    Integer numberOfJobs;
    Office branchDependency;

    public OfficeDTO(Office office) {
        this.id = office.getId();
        this.type = office.getType();
        this.name = office.getName();
        this.address = office.getAddress();
        this.numberOfJobs = office.getNumberOfJobs();
        this.branchDependency = office.getBranchDependency();
    }

}
