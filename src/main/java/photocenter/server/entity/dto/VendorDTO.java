package photocenter.server.entity.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.GoodTypes;
import photocenter.server.entity.model.Vendor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VendorDTO {

    String id;
    String company;
    GoodTypes goodTypes;

    public VendorDTO(Vendor vendor) {
        this.id = vendor.getId();
        this.company = vendor.getCompany();
        this.goodTypes = vendor.getGoodTypes();
    }
}
