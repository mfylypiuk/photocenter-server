package photocenter.server.entity.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;
import photocenter.server.entity.model.Sale;
import photocenter.server.entity.model.Warehouse;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SaleDTO {

    String id;
    Warehouse product;
    Integer count;
    Double price;
    Date date;
    Office shop;

    public SaleDTO(Sale sale) {
        this.id = sale.getId();
        this.product = sale.getProduct();
        this.count = sale.getCount();
        this.price = sale.getPrice();
        this.date = sale.getDate();
        this.shop = sale.getShop();
    }
}
