package photocenter.server.entity.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Vendor;
import photocenter.server.entity.model.Warehouse;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WarehouseDTO {

    String id;
    String product;
    String productType;
    Double price;
    Integer count;
    String company;
    Vendor vendor;

    public WarehouseDTO(Warehouse warehouse){
        this.id = warehouse.getId();
        this.product = warehouse.getProduct();
        this.productType = warehouse.getProductType();
        this.price = warehouse.getPrice();
        this.count = warehouse.getCount();
        this.company = warehouse.getCompany();
        this.vendor = warehouse.getVendor();

    }
}
