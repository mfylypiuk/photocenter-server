package photocenter.server.entity.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Office;
import photocenter.server.entity.model.Staff;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StaffDTO {

    String id;
    String name;
    String post;
    Office office;

    public StaffDTO(Staff staff) {
        this.id = staff.getId();
        this.name = staff.getName();
        this.post = staff.getPost();
        this.office = staff.getOffice();
    }
}
