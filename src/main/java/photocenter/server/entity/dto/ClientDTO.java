package photocenter.server.entity.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.Client;
import photocenter.server.entity.model.Office;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClientDTO {

    String id;
    String name;
    String status;
    Boolean discount;
    Office office;
    Integer numberOfOrders;

    public ClientDTO(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.status = client.getStatus();
        this.discount = client.getDiscount();
        this.office = client.getOffice();
        this.numberOfOrders = 0;
    }
}
