package photocenter.server.entity.dto;

import com.mongodb.lang.NonNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import photocenter.server.entity.model.DeliveryLog;
import photocenter.server.entity.model.Vendor;
import photocenter.server.entity.model.Warehouse;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DeliveryLogDTO {

    String id;
    Vendor vendor;
    Warehouse product;
    Integer count;
    Date date;

    public DeliveryLogDTO(DeliveryLog deliveryLog){
        this.id = deliveryLog.getId();
        this.vendor = deliveryLog.getVendor();
        this.product = deliveryLog.getProduct();
        this.count = deliveryLog.getCount();
        this.date = deliveryLog.getDate();
    }
}
