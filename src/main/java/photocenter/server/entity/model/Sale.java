package photocenter.server.entity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Sale")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Sale {
    @Id
    String id;

    @NonNull
    Warehouse product;

    @NonNull
    Integer count;

    @NonNull
    Double price;

    @NonNull
    Date date;

    @NonNull
    Office shop;

    public Sale(Warehouse product, Integer count, Double price, Date date, Office shop) {
        this.product = product;
        this.count = count;
        this.price = price;
        this.date = date;
        this.shop = shop;
    }
}
