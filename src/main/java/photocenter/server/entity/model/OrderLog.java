package photocenter.server.entity.model;

import java.util.Date;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "OrderLog")
@Getter @Setter
@NoArgsConstructor
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderLog {
    @Id
    @NonNull String id;
    @NonNull Order order;
    @NonNull String status;
    @NonNull Date date;
}
