package photocenter.server.entity.model;

import com.mongodb.lang.NonNull;
import lombok.*;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.experimental.FieldDefaults;

@Document(collection = "ConsumablesOrder")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConsumablesOrder {

    @Id
    String id;

    @NonNull
    String type;

    @NonNull
    Date date;

    @NonNull
    Integer count;

    @NonNull
    Office office;

}
