package photocenter.server.entity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Print")
@Getter @Setter
@NoArgsConstructor
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Print {
    @Id
    String id;
    @NonNull String formatType;
    @NonNull String paperType;
    @NonNull Integer numberOfPhotos;
    @NonNull Integer numberOfPhotosFromEachFrame;
}
