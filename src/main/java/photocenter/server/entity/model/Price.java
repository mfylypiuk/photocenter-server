package photocenter.server.entity.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Price")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Price {

    private static Price _priceList = null;
    public static synchronized Price getPriceList() {
        if (_priceList == null)
            _priceList = new Price();
        return _priceList;
    }

    Double cfUrgency;
    Double cfClientDiscount;
    Integer numberOfPhotosForDiscount;
    Double cfBigOrderDiscount;

    Double print;
    Double cfA0Format;
    Double cfA1Format;
    Double cfA2Format;
    Double cfA3Format;
    Double cfA4Format;
    Double cfA5Format;
    Double cfA6Format;
    Double cfPlainPaper;
    Double cfPhotoPaper;

    Double shots;
    Double cfPaidDevelopment;

    Double service;
    Double photoForDocument;
    Double photoForRestavration;
    Double artPhoto;
    Double camera;
    Double personalPhotographer;

    private Price() {
        this.cfUrgency = 2.0;
        this.cfClientDiscount = 0.8;
        this.numberOfPhotosForDiscount = 100;
        this.cfBigOrderDiscount = 0.9;
        this.print = 1.0;
        this.cfA0Format = 32.0;
        this.cfA1Format = 16.0;
        this.cfA2Format = 8.0;
        this.cfA3Format = 4.0;
        this.cfA4Format = 2.0;
        this.cfA5Format = 1.0;
        this.cfA6Format = 0.5;
        this.cfPlainPaper = 1.0;
        this.cfPhotoPaper = 5.0;
        this.shots = 2.5;
        this.cfPaidDevelopment = 0.0;
        this.service = 1.0;
        this.photoForDocument = 3.0;
        this.photoForRestavration = 50.0;
        this.artPhoto = 20.0;
        this.camera = 300.0;
        this.personalPhotographer = 1000.0;
    }
}