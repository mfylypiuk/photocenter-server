package photocenter.server.entity.model;

import com.mongodb.lang.NonNull;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Client")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Client {

    @Id
    String id;

    @NonNull
    String name;

    @NonNull
    String status;

    @NonNull
    Boolean discount;

    Office office;

    Integer numberOfOrders;

    public Client(String name, String status, Boolean discount, Office office) {
        this.name = name;
        this.status = status;
        this.discount = discount;
        this.office = office;
        this.numberOfOrders = 0;
    }

}