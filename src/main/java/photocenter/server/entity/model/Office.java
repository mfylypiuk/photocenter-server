package photocenter.server.entity.model;

import com.mongodb.lang.NonNull;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Office")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Office {

    @Id
    String id;

    @NonNull
    String type;

    @NonNull
    String name;

    @NonNull
    String address;

    @NonNull
    Integer numberOfJobs;

    Office branchDependency; // не обязательно (онли для киосков)

    public Office(String type, String name, String address, Integer numberOfJobs) {
        this.type = type;
        this.name = name;
        this.address = address;
        this.numberOfJobs = numberOfJobs;
    }

    public Office(String type, String name, String address, Integer numberOfJobs, Office branchDependency) {
        this.type = type;
        this.name = name;
        this.address = address;
        this.numberOfJobs = numberOfJobs;
        this.branchDependency = branchDependency;
    }

}