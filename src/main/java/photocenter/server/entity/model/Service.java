package photocenter.server.entity.model;
import lombok.*;

import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Service")
@Getter @Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Service {
    /*
    ид, тип, цена - обязательные поля
    модель (фотоаппарата), количество фото на документы,
    количество фото для реставрации, персональный фотограф,
    количество художественных фото - нет
     */
    @Id
    String id;

    Integer numberOfPhotoForDocument;
    Integer numberOfPhotoForRestavration;
    Integer numberOfArtPhotos;
    boolean camera;
    boolean personalPhotographer;

    public Service(Integer numberOfPhotoForDocument, Integer numberOfPhotoForRestavration, Integer numberOfArtPhotos, boolean camera, boolean personalPhotographer) {
        this.numberOfPhotoForDocument = numberOfPhotoForDocument;
        this.numberOfPhotoForRestavration = numberOfPhotoForRestavration;
        this.numberOfArtPhotos = numberOfArtPhotos;
        this.camera = camera;
        this.personalPhotographer = personalPhotographer;
    }
}
