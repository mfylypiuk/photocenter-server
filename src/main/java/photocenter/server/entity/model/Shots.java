package photocenter.server.entity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Shots")
@Getter @Setter
@NoArgsConstructor
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Shots {
    @Id
    String id;
    @NonNull Integer count;
    @NonNull Boolean paidDevelopment; // плёнка куплена ТУТ
}
