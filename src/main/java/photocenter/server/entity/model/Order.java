package photocenter.server.entity.model;

import com.mongodb.lang.NonNull;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Order")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Order {
    @Id
    String id;

    Double price;

    Client client;

    Office office;

    Print print;

    Shots shots;

    Boolean urgency;

    Service service;

    Date date;

    boolean done;

    boolean taken;

    public Order(Double price, Client client, Office office, Print print, Shots shots, Boolean urgency, Service service, Date date, boolean isDone, boolean isTaken) {
        if(date == null) {
            this.date = new Date();
        } else {
            this.date = date;
        }

        this.price = price;
        this.client = client;
        this.office = office;
        this.print = print;
        this.shots = shots;
        this.urgency = urgency;
        this.service = service;
        this.done = isDone;
        this.taken = isTaken;
    }
}
