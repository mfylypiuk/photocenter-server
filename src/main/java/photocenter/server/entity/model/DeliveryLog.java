package photocenter.server.entity.model;

import com.mongodb.lang.NonNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "DeliveryLog")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DeliveryLog {
    @Id
    String id;

    Vendor vendor;

    Warehouse product;

    @NonNull
    Integer count;

    Date date;
}
