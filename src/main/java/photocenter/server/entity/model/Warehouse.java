package photocenter.server.entity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Warehouse")
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Warehouse {

    @Id
    String id;

    @NonNull
    String product;

    @NonNull
    String productType;

    @NonNull
    Double price;

    @NonNull
    Integer count;

    @NonNull
    String company;

    @NonNull
    Vendor vendor;

}
