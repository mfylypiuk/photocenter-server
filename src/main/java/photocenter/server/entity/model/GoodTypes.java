package photocenter.server.entity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "GoodTypes")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GoodTypes {

    @Id
    String id;

    @NonNull
    boolean films;

    @NonNull
    boolean cameras;

    @NonNull
    boolean alboms;

    @NonNull
    boolean photopapers;

    @NonNull
    boolean chemicalReagents;
}
